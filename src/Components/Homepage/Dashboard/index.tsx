import React from "react"
import Carousal from 'react-material-ui-carousel'
import Item from './Item/index'
import home1 from '../../../Public/img/dashboard-img.png'
import home2 from '../../../Public/img/homepage-2.png'
import home3 from '../../../Public/img/homepage3.png'

const Dashboard = () => {
  var items = [
    {
      name1: "HUMAN ",
      name2: "CONNECTION",
      description: "Discover original, handmade designs crafted to give your home a personality.",
      image: home1
    },
    {
      name1: "QUALITY",
      name2: "ABOVE ALL",
      description: "Experience the warmth of best cotton to be ever woven into sheets.",
      image: home2
    },
    {
      name1: "FACTORY",
      name2: "DIRECT",
      description: "Taste the affordability of luxurious in-house manufactured products.",
      image: home3
    }
  ]
  return (
    <div>
      <Carousal>
        {
          items.map((item, i) => <Item key={i} item={item} />)
        }
      </Carousal>
    </div>
  )
}

export default Dashboard
