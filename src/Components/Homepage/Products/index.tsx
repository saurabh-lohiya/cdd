import React from "react"
import { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import Product from '../Product/index'

import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import "./index.scss"
import { Box } from "@mui/system";
import { Typography } from "@mui/material";

const Products = () => {
  const products = [
    {
      name: "Everyday Basics - Rose Printed",
      description: "Includes 1 fitted sheet",
      discountPrice: "$42.99",
      originalPrice: "$52.99",
      discount: "20%",
      image: "https://picsum.photos/300/350"
    },
    {
      name: "Everyday Basics - Rose Printed",
      description: "Includes 1 fitted sheet",
      discountPrice: "$42.99",
      originalPrice: "$52.99",
      discount: "20%",
      image: "https://picsum.photos/300/350"
    },
    {
      name: "Everyday Basics - Rose Printed",
      description: "Includes 1 fitted sheet",
      discountPrice: "$42.99",
      originalPrice: "$52.99",
      discount: "20%",
      image: "https://picsum.photos/300/350"
    },
    {
      name: "Everyday Basics - Rose Printed",
      description: "Includes 1 fitted sheet",
      discountPrice: "$42.99",
      originalPrice: "$52.99",
      discount: "20%",
      image: "https://picsum.photos/300/350"
    },
    {
      name: "Everyday Basics - Rose Printed",
      description: "Includes 1 fitted sheet",
      discountPrice: "$42.99",
      originalPrice: "$52.99",
      discount: "20%",
      image: "https://picsum.photos/300/350"
    }, {
      name: "Everyday Basics - Rose Printed",
      description: "Includes 1 fitted sheet",
      discountPrice: "$42.99",
      originalPrice: "$52.99",
      discount: "20%",
      image: "https://picsum.photos/300/350"
    },
  ]
  return (
    <Box>
      <Typography variant="h6" fontWeight={600} align="left">PRODUCT CATEGORIES</Typography>
      <Typography variant="h2" fontWeight={600} align="left">Bedsheets</Typography>
      <Swiper
        className="swiper-div"
        modules={[Navigation, Pagination, Scrollbar, A11y]}
        spaceBetween={40}
        slidesPerView={4}
        navigation
        pagination={{ clickable: true }}
        // scrollbar={{ draggable: true }}
        onSwiper={(swiper) => console.log(swiper)}
        onSlideChange={() => console.log('slide change')}
      >
        {products.map((product, i) => <SwiperSlide key={i}><Product item={product} /></SwiperSlide>)}
      </Swiper>
    </Box>
  )
}

export default Products
