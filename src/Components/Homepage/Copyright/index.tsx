import { Box } from "@mui/system"
import React from "react"
import './index.scss'

const Copyright = () => {
  return (
    <Box sx={{ padding: "1.25rem 0", background: 'lightgrey' }}>
      California Design Den All Rights Reserved
    </Box>
  )
}

export default Copyright
